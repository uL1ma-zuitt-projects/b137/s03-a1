package b137.obrera.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args){
        System.out.println("Factorial\n");

        Scanner appNumber = new Scanner(System.in);

        System.out.println("What number do you want to factor?");
        int factorial = 1;
        int number = appNumber.nextInt();

        for (int i = 1; i <= number; i++){
            factorial *= i;
        }
        System.out.println("Factorial of " + number + " is: " + factorial);
    }
}
